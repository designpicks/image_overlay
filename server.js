const express = require('express');
const fileUpload = require('express-fileupload');
const { exec } = require('child_process');
const app = express();
const port = 3000;

// express-fileupload 미들웨어 사용 설정
app.use(fileUpload());

// 정적 파일(예: HTML, JS, CSS)을 제공하기 위한 설정
app.use(express.static('public'));

// 이미지 합성을 위한 라우트 설정
app.post('/combine-images', (req, res) => {
    if (!req.files || !req.files.backgroundImage || !req.files.overlayImage) {
        return res.status(400).send('이미지가 업로드되지 않았습니다.');
    }

    let backgroundImage = req.files.backgroundImage;
    let overlayImage = req.files.overlayImage;
    let outputImagePath = './public/output/outputImage.png'; // 결과 이미지 경로

    // 임시 경로에 이미지 저장
    backgroundImage.mv('./public/uploads/background.png', (err) => {
        if (err) return res.status(500).send(err);
        overlayImage.mv('./public/uploads/overlay.png', (err) => {
            if (err) return res.status(500).send(err);

            // 오버레이 이미지 리사이즈 및 페더 효과 적용
            exec(`convert ./public/uploads/overlay.png -resize 80% -bordercolor transparent -border 20x20 -background transparent -alpha set -virtual-pixel transparent -channel A -blur 0x8 -level 50%,100% +channel ./public/uploads/resized_overlay.png`, (err, stdout, stderr) => {
                if (err) {
                    console.log(`exec error: ${err}`);
                    return res.status(500).send(err);
                }

                // 리사이즈 및 페더 효과가 적용된 오버레이 이미지와 배경 이미지를 합성
                exec(`composite -gravity center ./public/uploads/resized_overlay.png ./public/uploads/background.png ${outputImagePath}`, (err, stdout, stderr) => {
                    if (err) {
                        console.log(`exec error: ${err}`);
                        return res.status(500).send(err);
                    }

                    // 합성된 이미지 경로 반환
                    res.send({ path: 'output/outputImage.png' });
                });
            });
        });
    });
});

app.listen(port, () => {
    console.log(`서버가 http://localhost:${port}에서 실행 중입니다.`);
});

